<?php

// interface total{
// 	public function total($total);
// }

class Plod{
	public $ime;

	public function __construct($ime){
		$this->ime = $ime;
	}
}

class Parche extends Plod{

	public $parche;

	public function __construct($parche, $ime){
		$this->parche = $parche;
		parent:: __construct($ime);
	}

	public function cena($parche, $cenaa){
		$this->cenaa = $cenaa;
		$vkcena = $this->parche*$cenaa;
		return $vkcena;
	}
}

class Kilo extends Plod{

	public $kilo;

	public function __construct($kilo, $ime){
		$this->kilo = $kilo;
		parent:: __construct($ime);
	}

	public function cena($kilo, $cena){
		$this->cena = $cena;
		$vkcena = $kilo*$cena;
		return $vkcena;
	}


}
